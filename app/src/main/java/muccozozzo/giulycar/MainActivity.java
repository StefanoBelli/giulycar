package muccozozzo.giulycar;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.view.MenuItem;
import android.util.SparseArray;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {
    private SparseArray<String> propertyCommands;
    private final int PROPERTIES = 12;

    private String stopTurnLeft;
    private String stopTurnRight;
    private String stopBack;
    private String stopGo;

    private String stopTurnLeftCommand;
    private String stopTurnRightCommand;
    private String stopGoCommand;
    private String stopBackCommand;
    private String turnLeftCommand;
    private String turnRightCommand;
    private String goCommand;
    private String backCommand;
    private String emergencyLightToggleCommand;
    private String rightLightToggleCommand;
    private String leftLightToggleCommand;
    private String clacsonCommand;

    private TextView connectionStatusTv;
    private TextView lastPressedVeichleKeyTv;
    private TextView emergencyStatusTv;
    private TextView clacsonStatusTv;
    private TextView rightArrowStatusTv;
    private TextView leftArrowStatusTv;

    private Button leftButton;
    private Button rightButton;
    private Button accellButton;
    private Button backButton;
    private Button clacsonButton;
    private Button rightLightButton;
    private Button leftLightButton;
    private Button emergencyButton;

    private SharedPreferences appCommands;

    private MediaPlayer accellVePlayer;
    private MediaPlayer clacsonVePlayer;
    private MediaPlayer connectVePlayer;
    private MediaPlayer emergencyVePlayer;
    private MediaPlayer backVePlayer;

    private boolean veVoice;

    private boolean leftLights;
    private boolean rightLights;
    private boolean emergencyLights;

    private BluetoothSocket sharedBtSocket;
    private OutputStream outputBtSocketStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        accellVePlayer = MediaPlayer.create(this,R.raw.accell_ve);
        clacsonVePlayer = MediaPlayer.create(this,R.raw.clacson_ve);
        connectVePlayer = MediaPlayer.create(this,R.raw.connect_ve);
        emergencyVePlayer = MediaPlayer.create(this,R.raw.emergenza_ve);
        backVePlayer = MediaPlayer.create(this,R.raw.retromarcia_ve);

        stopTurnLeft = CarCommands.makeStop(CarCommands.TURN_LEFT);
        stopTurnRight = CarCommands.makeStop(CarCommands.TURN_RIGHT);
        stopBack = CarCommands.makeStop(CarCommands.BACK);
        stopGo = CarCommands.makeStop(CarCommands.GO);

        leftButton = (Button) findViewById(R.id.turnLeft);
        rightButton = (Button) findViewById(R.id.turnRight);
        accellButton = (Button) findViewById(R.id.go);
        backButton = (Button) findViewById(R.id.stopOrBack);
        clacsonButton = (Button) findViewById(R.id.clacsonPress);
        rightLightButton = (Button) findViewById(R.id.rightArrowToggle);
        leftLightButton = (Button) findViewById(R.id.leftArrowToggle);
        emergencyButton = (Button) findViewById(R.id.emergencyArrowsLightToggle);

        connectionStatusTv = (TextView) findViewById(R.id.connectionStatus);
        lastPressedVeichleKeyTv = (TextView) findViewById(R.id.lastAction);
        emergencyStatusTv = (TextView) findViewById(R.id.emergencyLightView);
        clacsonStatusTv = (TextView) findViewById(R.id.clacsonView);
        rightArrowStatusTv = (TextView) findViewById(R.id.rightLightView);
        leftArrowStatusTv = (TextView) findViewById(R.id.leftLightView);

        //TODO: when connected, check if commands are null
        //TODO: if null, then disable buttons

        appCommands = getApplicationContext().getSharedPreferences("CarCommands",MODE_PRIVATE);
        appCommands.registerOnSharedPreferenceChangeListener(new SharedPrefsChanged());
        turnLeftCommand = appCommands.getString(CarCommands.TURN_LEFT, null);
        turnRightCommand = appCommands.getString(CarCommands.TURN_RIGHT, null);
        goCommand = appCommands.getString(CarCommands.GO, null);
        backCommand = appCommands.getString(CarCommands.BACK, null);
        emergencyLightToggleCommand = appCommands.getString(CarCommands.EMERGENCY_TOG, null);
        rightLightToggleCommand = appCommands.getString(CarCommands.RIGHT_LIGHT_TOG, null);
        leftLightToggleCommand = appCommands.getString(CarCommands.LEFT_LIGHT_TOG, null);
        clacsonCommand = appCommands.getString(CarCommands.CLACSON, null);
        stopTurnLeftCommand = appCommands.getString(stopTurnLeft,null);
        stopTurnRightCommand = appCommands.getString(stopTurnRight, null);
        stopBackCommand = appCommands.getString(stopBack,null);
        stopGoCommand = appCommands.getString(stopGo,null);

        leftButton.setOnTouchListener(new ButtonTouchAction(TargetButton.BUTTON_LEFT));
        rightButton.setOnTouchListener(new ButtonTouchAction(TargetButton.BUTTON_RIGHT));
        accellButton.setOnTouchListener(new ButtonTouchAction(TargetButton.BUTTON_ACCEL));
        backButton.setOnTouchListener(new ButtonTouchAction(TargetButton.BUTTON_BACK));
        clacsonButton.setOnClickListener(new ButtonClickAction(TargetButton.BUTTON_CLACSON));
        rightLightButton.setOnClickListener(new ButtonClickAction(TargetButton.BUTTON_RIGHT_LIGHT));
        leftLightButton.setOnClickListener(new ButtonClickAction(TargetButton.BUTTON_LEFT_LIGHT));
        clacsonButton.setOnTouchListener(new ButtonTouchAction(TargetButton.BUTTON_CLACSON));
        emergencyButton.setOnClickListener(new ButtonClickAction(TargetButton.BUTTON_EMERGENCY));

        veVoice = false;
        propertyCommands = new SparseArray<>(PROPERTIES);
        propertyCommands.append(0,CarCommands.TURN_LEFT);
        propertyCommands.append(1,CarCommands.TURN_RIGHT);
        propertyCommands.append(2,CarCommands.GO);
        propertyCommands.append(3,CarCommands.BACK);
        propertyCommands.append(4,CarCommands.CLACSON);
        propertyCommands.append(5,CarCommands.RIGHT_LIGHT_TOG);
        propertyCommands.append(6,CarCommands.LEFT_LIGHT_TOG);
        propertyCommands.append(7,CarCommands.EMERGENCY_TOG);
        propertyCommands.append(8,stopTurnLeft);
        propertyCommands.append(9,stopTurnRight);
        propertyCommands.append(10,stopGo);
        propertyCommands.append(11,stopBack);

        setStatusTextViewsVisibility(View.INVISIBLE);

        leftLights = false;
        rightLights = false;
        emergencyLights = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        btSockWrite("goodbye");

        try {
            if(outputBtSocketStream != null) {
                outputBtSocketStream.flush();
                outputBtSocketStream.close();
            }

            if(sharedBtSocket != null)
                sharedBtSocket.close();
        } catch(IOException ioexcp) {
            Toast.makeText(MainActivity.this,R.string.unableClose,Toast.LENGTH_SHORT).show();
        }
    }

    private void setStatusTextViewsVisibility(int flag) {
        emergencyStatusTv.setVisibility(flag);
        lastPressedVeichleKeyTv.setVisibility(flag);
        clacsonStatusTv.setVisibility(flag);
        rightArrowStatusTv.setVisibility(flag);
        leftArrowStatusTv.setVisibility(flag);
    }

    private void setControlButtonsEnabled(boolean flag) {
        leftButton.setEnabled(flag);
        rightButton.setEnabled(flag);
        accellButton.setEnabled(flag);
        backButton.setEnabled(flag);
        clacsonButton.setEnabled(flag);
        emergencyButton.setEnabled(flag);
        rightLightButton.setEnabled(flag);
        leftLightButton.setEnabled(flag);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    private void requestBluetoothPermissions() {
        final int CODE = 5;

        String[] permissionsToRequest = {
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.BLUETOOTH,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        };

        boolean allPermissionsGranted = true;

        for(String permission : permissionsToRequest)
            allPermissionsGranted = allPermissionsGranted &&
                    (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED);

        if(!allPermissionsGranted)
            ActivityCompat.requestPermissions(this, permissionsToRequest, CODE);
    }

    private void btSockWrite(String data) {
        if(outputBtSocketStream == null) {
            Toast.makeText(this, R.string.nullSockets + '\n', Toast.LENGTH_LONG).show();
            return;
        }

        try {
            outputBtSocketStream.write(data.getBytes());
        } catch(IOException ioe) {
            Toast.makeText(this, R.string.writeError, Toast.LENGTH_SHORT).show();
        }
    }

    private BluetoothSocket getSocketByDeviceString(BluetoothAdapter btAdapt, String identifier)
            throws IOException {
        BluetoothDevice device = btAdapt.getRemoteDevice(identifier.substring(identifier.length() - 17));
        return device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
    }

    public void onClickConnectDevice(MenuItem v) {
        requestBluetoothPermissions();

        View view = LayoutInflater.from(getApplicationContext())
                .inflate(R.layout.dialog_connect_btdevice, null);

        final Vector<String> deviceNames = new Vector<>();
        final ArrayAdapter<String> devicesNames = new ArrayAdapter<>
                (this,android.R.layout.simple_list_item_1,deviceNames);
        final ProgressBar progress = view.findViewById(R.id.btSearching);

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!btAdapter.isEnabled()) {
            btAdapter.enable();
            while (!btAdapter.isEnabled()){}
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_NAME_CHANGED);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        builder.setCancelable(false);

        ListView devicesListView = view.findViewById(R.id.availBtDevices);
        Button triggerStartDiscovery = view.findViewById(R.id.scanDevices);

        triggerStartDiscovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btAdapter.startDiscovery();
            }
        });

        final Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
        for(final BluetoothDevice dev : pairedDevices)
            deviceNames.add(dev.getName() + " " + dev.getAddress());

        final BroadcastReceiver btEvent = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action))
                    progress.setVisibility(View.VISIBLE);
                else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action))
                    progress.setVisibility(View.INVISIBLE);
                else if(BluetoothDevice.ACTION_NAME_CHANGED.equals(action)) {
                    BluetoothDevice dev = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    String str = dev.getName() + " " + dev.getAddress();
                    if(!deviceNames.contains(str)) {
                        deviceNames.add(str);
                        devicesNames.notifyDataSetChanged();
                    }
                }
            }
        };

        devicesListView.setAdapter(devicesNames);
        registerReceiver(btEvent,filter);

        builder.setNegativeButton(R.string.closeDialog, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                unregisterReceiver(btEvent);
                btAdapter.cancelDiscovery();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();

        devicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    sharedBtSocket = getSocketByDeviceString(btAdapter, deviceNames.get((int) l));
                    sharedBtSocket.connect();
                    outputBtSocketStream = sharedBtSocket.getOutputStream();
                } catch(IOException ioe) {
                    Toast.makeText(MainActivity.this, R.string.ioexcp, Toast.LENGTH_SHORT).show();
                    return;
                }

                if(veVoice)
                    connectVePlayer.start();

                connectionStatusTv.setText(sharedBtSocket.getRemoteDevice().getName());
                setStatusTextViewsVisibility(View.VISIBLE);
                setControlButtonsEnabled(true);
                unregisterReceiver(btEvent);
                btAdapter.cancelDiscovery();
                dialog.dismiss();

                Toast
                        .makeText(MainActivity.this,R.string.successConnected,Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    public void onClickEditCommands(MenuItem v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(getApplicationContext())
                .inflate(R.layout.dialog_edit_command, null);
        builder.setTitle(R.string.commandEditorTitle);
        builder.setView(view);
        builder.setCancelable(false);

        final EditText startCommandField = view.findViewById(R.id.startCommand);
        final EditText stopCommandField = view.findViewById(R.id.stopCommand);
        final Spinner propertyChooser = view.findViewById(R.id.whichCommandSelector);

        propertyChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                startCommandField.setText("");
                stopCommandField.setText("");
                startCommandField.setEnabled(true);

                if(l == 0) {
                    if(turnLeftCommand != null)
                        startCommandField.setText(turnLeftCommand);

                    if(stopTurnLeftCommand != null)
                        stopCommandField.setText(stopTurnLeftCommand);

                    stopCommandField.setEnabled(true);
                } else if(l == 1) {
                    if(turnRightCommand != null)
                        startCommandField.setText(turnRightCommand);

                    if(stopTurnRightCommand != null)
                        stopCommandField.setText(stopTurnRightCommand);

                    stopCommandField.setEnabled(true);
                } else if(l == 2) {
                    if(goCommand != null)
                        startCommandField.setText(goCommand);

                    if(stopGoCommand != null)
                        stopCommandField.setText(stopGoCommand);

                    stopCommandField.setEnabled(true);
                } else if(l == 3) {
                    if(backCommand != null)
                        startCommandField.setText(backCommand);

                    if(stopBackCommand != null)
                        stopCommandField.setText(stopBackCommand);

                    stopCommandField.setEnabled(true);
                } else if(l == 4) {
                    if(clacsonCommand != null)
                        startCommandField.setText(clacsonCommand);

                    stopCommandField.setEnabled(false);
                    stopCommandField.setText("");
                } else if(l == 5) {
                    if(rightLightToggleCommand != null)
                        startCommandField.setText(rightLightToggleCommand);

                    stopCommandField.setEnabled(false);
                    stopCommandField.setText("");
                } else if(l == 6) {
                    if(leftLightToggleCommand != null)
                        startCommandField.setText(leftLightToggleCommand);

                    stopCommandField.setEnabled(false);
                    stopCommandField.setText("");
                } else if(l == 7) {
                    if(emergencyLightToggleCommand!= null)
                        startCommandField.setText(emergencyLightToggleCommand);

                    stopCommandField.setEnabled(false);
                    stopCommandField.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                startCommandField.setText("");
                stopCommandField.setText("");
                startCommandField.setEnabled(false);
                stopCommandField.setEnabled(false);
            }
        });

        builder.setPositiveButton(R.string.applyChanges, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SharedPreferences.Editor prefsEditor = appCommands.edit();
                int selectedId = (int) propertyChooser.getSelectedItemId();

                prefsEditor
                        .putString(propertyCommands.get(selectedId),startCommandField
                                                            .getText().toString())
                        .apply();

                if(selectedId <= 3) {
                    prefsEditor
                            .putString(propertyCommands.get(selectedId + 8), stopCommandField
                                    .getText().toString())
                            .apply();
                }
            }
        });

        builder.setNegativeButton(R.string.closeDialog, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.create().show();
    }

    public void onClickToggleVeVoice(MenuItem v) {
        veVoice = !veVoice;
        v.setTitle(veVoice ? R.string.veVoiceEnabled : R.string.veVoiceDisabled);
    }

    private class SharedPrefsChanged implements SharedPreferences.OnSharedPreferenceChangeListener {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if(key.equals(CarCommands.TURN_LEFT))
                turnLeftCommand = sharedPreferences.getString(CarCommands.TURN_LEFT, null);
            else if(key.equals(CarCommands.TURN_RIGHT))
                turnRightCommand = sharedPreferences.getString(CarCommands.TURN_RIGHT, null);
            else if(key.equals(CarCommands.GO))
                goCommand = sharedPreferences.getString(CarCommands.GO, null);
            else if(key.equals(CarCommands.BACK))
                backCommand = sharedPreferences.getString(CarCommands.BACK, null);
            else if(key.equals(CarCommands.CLACSON))
                clacsonCommand = sharedPreferences.getString(CarCommands.CLACSON,null);
            else if(key.equals(CarCommands.RIGHT_LIGHT_TOG))
                rightLightToggleCommand = sharedPreferences.getString(CarCommands.RIGHT_LIGHT_TOG,null);
            else if(key.equals(CarCommands.LEFT_LIGHT_TOG))
                leftLightToggleCommand = sharedPreferences.getString(CarCommands.LEFT_LIGHT_TOG,null);
            else if(key.equals(stopBack))
                stopBackCommand = sharedPreferences.getString(stopBack,null);
            else if(key.equals(stopGo))
                stopGoCommand = sharedPreferences.getString(stopGo,null);
            else if(key.equals(stopTurnRight))
                stopTurnRightCommand = sharedPreferences.getString(stopTurnRight,null);
            else if(key.equals(stopTurnLeft))
                stopTurnLeftCommand = sharedPreferences.getString(stopTurnLeft,null);
            else if(key.equals(CarCommands.EMERGENCY_TOG))
                emergencyLightToggleCommand = sharedPreferences.getString(CarCommands.EMERGENCY_TOG,null);

            Toast.makeText(MainActivity.this,R.string.appliedNewPrefs,Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private class ButtonTouchAction implements View.OnTouchListener {
        private final TargetButton button;

        private ButtonTouchAction(final TargetButton targetButton) {
            button = targetButton;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if(event.getAction() == MotionEvent.ACTION_DOWN) {
                if (button == TargetButton.BUTTON_CLACSON) {
                    btSockWrite(clacsonCommand);
                    lastPressedVeichleKeyTv.setText(R.string.clacson);
                    clacsonStatusTv.setText(R.string.carStatusClacsonOn);
                    if(veVoice) {
                        if (clacsonVePlayer.isPlaying()) {
                            clacsonVePlayer.pause();
                            clacsonVePlayer.seekTo(0);
                        }
                        clacsonVePlayer.start();
                    }
                } else if(button == TargetButton.BUTTON_ACCEL) {
                    btSockWrite(goCommand);
                    lastPressedVeichleKeyTv.setText(R.string.nowAccell);
                    if(veVoice) {
                        accellVePlayer.setLooping(true);
                        accellVePlayer.start();
                    }
                } else if(button == TargetButton.BUTTON_BACK) {
                    btSockWrite(backCommand);
                    lastPressedVeichleKeyTv.setText(R.string.stopOrBack);
                    if(veVoice) {
                        backVePlayer.setLooping(true);
                        backVePlayer.start();
                    }
                } else if(button == TargetButton.BUTTON_RIGHT) {
                    btSockWrite(turnRightCommand);
                    lastPressedVeichleKeyTv.setText(R.string.right);
                } else if(button == TargetButton.BUTTON_LEFT) {
                    btSockWrite(turnLeftCommand);
                    lastPressedVeichleKeyTv.setText(R.string.left);
                }
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                v.performClick();
                if(button == TargetButton.BUTTON_ACCEL) {
                    btSockWrite(stopGoCommand);
                    if(veVoice) {
                        accellVePlayer.pause();
                        accellVePlayer.setLooping(false);
                        accellVePlayer.seekTo(0);
                    }
                } else if(button == TargetButton.BUTTON_BACK) {
                    btSockWrite(stopBackCommand);
                    if(veVoice) {
                        backVePlayer.pause();
                        backVePlayer.setLooping(false);
                        backVePlayer.seekTo(0);
                    }
                } else if(button == TargetButton.BUTTON_CLACSON) {
                    btSockWrite(clacsonCommand);
                    clacsonStatusTv.setText(R.string.carStatusClacsonOff);
                } else if(button == TargetButton.BUTTON_LEFT) {
                    btSockWrite(stopTurnLeftCommand);
                } else if(button == TargetButton.BUTTON_RIGHT) {
                    btSockWrite(stopTurnRightCommand);
                }
            }
            return true;
        }
    }

    private class ButtonClickAction implements View.OnClickListener {
        private final TargetButton button;

        private ButtonClickAction(final TargetButton targetButton) {
            button = targetButton;
        }

        @Override
        public void onClick(View view) {
            if (button == TargetButton.BUTTON_EMERGENCY) {
                btSockWrite(emergencyLightToggleCommand);
                emergencyLights = !emergencyLights;
                emergencyStatusTv.setText(emergencyLights ?
                        R.string.carStatusEmergencyOn :
                        R.string.carStatusEmergencyOff);
                lastPressedVeichleKeyTv.setText(R.string.emergency);

                if(!emergencyLights && veVoice) {
                        if (emergencyVePlayer.isPlaying()) {
                            emergencyVePlayer.pause();
                            emergencyVePlayer.seekTo(0);
                        }
                        emergencyVePlayer.start();
                }
            } else if(button == TargetButton.BUTTON_LEFT_LIGHT) {
                btSockWrite(leftLightToggleCommand);
                leftLights = !leftLights;
                leftArrowStatusTv.setText(leftLights ?
                        R.string.carStatusLeftLightOn :
                        R.string.carStatusLeftLightOff);
                lastPressedVeichleKeyTv.setText(R.string.leftLight);
            } else if(button == TargetButton.BUTTON_RIGHT_LIGHT) {
                btSockWrite(rightLightToggleCommand);
                rightLights = !rightLights;
                rightArrowStatusTv.setText(rightLights ?
                        R.string.carStatusRightLightOn :
                        R.string.carStatusRightLightOff);
                lastPressedVeichleKeyTv.setText(R.string.rightLight);
            }
        }
    }
}
