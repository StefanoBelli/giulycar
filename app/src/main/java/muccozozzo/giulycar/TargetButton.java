package muccozozzo.giulycar;

enum TargetButton {
    BUTTON_LEFT,
    BUTTON_RIGHT,
    BUTTON_ACCEL,
    BUTTON_BACK,
    BUTTON_CLACSON,
    BUTTON_RIGHT_LIGHT,
    BUTTON_LEFT_LIGHT,
    BUTTON_EMERGENCY
}
