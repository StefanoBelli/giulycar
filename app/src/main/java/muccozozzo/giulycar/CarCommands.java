package muccozozzo.giulycar;

import android.support.annotation.NonNull;

final class CarCommands {
    static final String TURN_LEFT        = "turnLeft";
    static final String TURN_RIGHT       = "turnRight";
    static final String GO               = "go";
    static final String BACK             = "back";
    static final String EMERGENCY_TOG    = "emergencyToggle";
    static final String RIGHT_LIGHT_TOG  = "rightLightToggle";
    static final String LEFT_LIGHT_TOG   = "leftLigthToggle";
    static final String CLACSON          = "clacson";

    @NonNull
    static String makeStop(String orig) {
        return "stop" + orig.substring(0,1).toUpperCase() + orig.substring(1);
    }
}
